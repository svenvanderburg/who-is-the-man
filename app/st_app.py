import streamlit as st

from app.client import Client


@st.cache(hash_funcs={Client: id}, allow_output_mutation=True)
def get_client():
    return Client()


client = get_client()

st.sidebar.text('Start nieuw spel. \n'
                'Alle items in de hoed worden \n'
                'definitief verwijderd')
if st.sidebar.button('Start nieuw spel'):
    print('Starting a new game, emptying database')
    client.empty()


game_state = st.sidebar.selectbox("Selecteer", ["De hoed vullen",
                                                "Het spel spelen"])

if game_state == 'De hoed vullen':
    new_item_name = st.text_input('Dier/persoon/ding', 'Obama')
    if st.button("Stop in de hoed"):
        print(f'Adding {new_item_name} to database')
        client.put(new_item_name)

elif game_state == 'Het spel spelen':
    if st.button("Volgende briefje!"):
        hat_item = client.get_random()
        st.text(hat_item.name)

