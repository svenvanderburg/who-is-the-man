from sqlalchemy import Integer, Column, String, Boolean
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class HatItem(Base):
    __tablename__ = 'hat_items'
    id = Column(Integer, primary_key=True)
    name = Column(String(length=256), index=True, nullable=False)
    in_hat = Column(Boolean(), default=True)
    in_game = Column(Boolean(), default=False)
