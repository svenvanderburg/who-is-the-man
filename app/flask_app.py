import os

from flask import Flask, render_template, request, redirect, jsonify

from app.client import Client

client = Client()


def create_app():
    app = Flask(__name__)
    SECRET_KEY = os.urandom(32)
    app.config['SECRET_KEY'] = SECRET_KEY

    @app.route("/", methods=['GET', 'POST'])
    def index():
        all_count, in_hat_count = client.count()
        if request.method == 'POST':
            if 'add_item' in request.form:
                item_name = request.form['item_name']
                client.put(item_name)
                return redirect('/')
            elif 'empty_hat' in request.form:
                client.empty()
                return redirect('/')
            elif 'start_game' in request.form:
                return redirect('/game')
            elif 'fill_hat' in request.form:
                client.fill_hat()
                return redirect('/')
        return render_template('index.html',
                               all_count=all_count,
                               in_hat_count=in_hat_count)

    @app.route("/game", methods=['GET'])
    def game():
        return render_template('game.html')

    @app.route("/next-item", methods=["POST"])
    def next_item():
        data = request.get_json()
        hat_items = [hat_item.name for hat_item
                     in client.get_in_game_items()]
        hat_item = client.get_random()
        if hat_item:
            hat_item = hat_item.name
        else:
            hat_item = 'Geen briefjes meer!'

        return jsonify({'current_item': hat_item,
                        'prev_items': ', '.join(hat_items),
                        'score': len(hat_items)})

    return app


def main():
    app = create_app()
    app.run()


if __name__ == '__main__':
    main()
