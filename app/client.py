import random

from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from app.data_models import HatItem


class Client:
    def __init__(self):
        engine = create_engine('sqlite:///whoistheman.db',
                               connect_args={'check_same_thread': False})
        self.session = Session(bind=engine)

    def put(self, item_name):
        new_item = HatItem(name=item_name, in_hat=True)
        self.session.add(new_item)
        self.session.commit()

    def empty(self):
        self.session.query(HatItem).delete()
        self.session.commit()

    def get_random(self):
        hat_items = self.session.query(HatItem).filter(HatItem.in_hat)
        if not hat_items.all():
            return
        random_item = random.choice(hat_items.all())
        random_item.in_hat = False
        random_item.in_game = True
        self.session.commit()
        # TODO: solution for no items left
        return random_item

    def get_in_game_items(self):
        return self.session.query(HatItem).filter(HatItem.in_game).all()

    def fill_hat(self):
        self.session.query(HatItem).update({HatItem.in_hat: True})
        self.session.query(HatItem).update({HatItem.in_game: False})
        self.session.commit()

    def count(self):
        all_items = self.session.query(HatItem).all()
        in_hat_count = len([item for item in all_items if item.in_hat])
        all_count = len(all_items)
        return all_count, in_hat_count
